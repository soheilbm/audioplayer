//
//  TimeHelper.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

struct TimeHelper {
    static func getDate(from string: String?, format: String = "yyyy-MM-dd'T'HH:mm:ssZ") -> Date? {
        guard let string = string else { return nil}
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: string)
    }
    
    static func toDateTime(_ date: Date?, format: String = "yyyy-MM-dd") -> String? {
        guard let date = date else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return  dateFormatter.string(from: date)
    }
}
