//
//  ImageHelper.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

extension UIImageView {
    func download(_ str: String?) {
        guard let str = str, let url = URL(string: str) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil,
                let _ = response as? HTTPURLResponse,
                let _ = response?.mimeType,
                let image = UIImage(data: data) else { return }
            
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
        }.resume()
    }
}
