//
//  DataParser.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

typealias AnyParse = [String: Any]

public protocol InputType {}
extension Int: InputType {}
extension String: InputType {}

public struct Parser {
    let data: AnyParse
    init(_ data: AnyParse) {self.data = data }
    
    public subscript(_ types: InputType...) -> Any? {
        return value(types, data)
    }
    
    private func value(_ types: [InputType],_ data: Any) -> Any? {
        guard let i = types.first else { return data}
        let newTypes = Array(types.dropFirst())
        
        if let dataParser = data as? AnyParse, let i = i as? String {
            if let val = dataParser[i] {
                return value(newTypes, val)
            }
        }
        
        if let dataParser = data as? Array<Any>, let i = i as? Int, dataParser.count > i {
            return value(newTypes, dataParser[i])
        }
        
        return nil
    }
}

extension Array where Element: Hashable {
    
    func after(item: Element) -> Element? {
        if let index = self.index(of: item), index + 1 < self.count {
            return self[index + 1]
        }
        return nil
    }
}
