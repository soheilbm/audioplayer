//
//  MusicManager.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation
import AVFoundation

enum PlayerStatus: String {
    case startPlaying = "playbackLikelyToKeepUp"
    case loadedTimeRanges
    case timedMetadata
    case status
    case paused
    case resumed
    case restart
}

final class MusicManager: NSObject {
    fileprivate let player: AVPlayer = AVPlayer()
    fileprivate var modelItems = [DataItem]()
    fileprivate var isPlaying = false
    fileprivate var currentItemIndex: Int?
    fileprivate var observers = [ObserverHolder]()
    
    fileprivate override init() {super.init()}
    
    fileprivate var playerItem: AVPlayerItem? {
        didSet {
            playerItem?.addObserver(self, forKeyPath: PlayerStatus.startPlaying.rawValue, options:[.new,.old], context: nil)
            playerItem?.addObserver(self, forKeyPath: PlayerStatus.loadedTimeRanges.rawValue, options: [.new,.old], context: nil)
            playerItem?.addObserver(self, forKeyPath: PlayerStatus.status.rawValue, options: [.new,.old], context: nil)
            playerItem?.addObserver(self, forKeyPath: PlayerStatus.timedMetadata.rawValue, options: [.new,.old], context: nil)
        }
        willSet {
            playerItem?.removeObserver(self, forKeyPath: PlayerStatus.startPlaying.rawValue)
            playerItem?.removeObserver(self, forKeyPath: PlayerStatus.loadedTimeRanges.rawValue)
            playerItem?.removeObserver(self, forKeyPath: PlayerStatus.status.rawValue)
            playerItem?.removeObserver(self, forKeyPath: PlayerStatus.timedMetadata.rawValue)
        }
    }
    
    fileprivate func observerHolders() -> [ObserverHolder] {
        return self.observers.filter{ $0.observer != nil }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == PlayerStatus.startPlaying.rawValue {
            isPlaying = true
            observerHolders().forEach{ $0.observer?.notify(with: .startPlaying) }
        }
        
        if keyPath == PlayerStatus.loadedTimeRanges.rawValue {
            observerHolders().forEach{ $0.observer?.notify(with: .loadedTimeRanges) }
        }
        
        if keyPath == PlayerStatus.status.rawValue {
            observerHolders().forEach{ $0.observer?.notify(with: .status) }
        }
        
        if keyPath == PlayerStatus.timedMetadata.rawValue {
            observerHolders().forEach{ $0.observer?.notify(with: .timedMetadata) }
        }
    }
    
    fileprivate func resumeOrPauseCurrentAudioItem() {
        if !isPlaying {
            player.play()
            isPlaying = true
            observerHolders().forEach { $0.observer?.notify(with: .resumed) }
        }else{
            player.pause()
            isPlaying = false
            observerHolders().forEach { $0.observer?.notify(with: .paused) }
        }
    }
    
    fileprivate func playSong(item: DataItem) {
        guard let url = URL(string: item.audioLink) else { return }
        
        playerItem = AVPlayerItem(url: url)
        currentItemIndex = modelItems.index(of: item)
        player.replaceCurrentItem(with: playerItem)
        player.volume = 1.0
        player.play()
    }
        
    fileprivate func restartSong() {
        player.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
        observerHolders().forEach{ $0.observer?.notify(with: .restart) }
    }
}

extension MusicManager {
    static let shared = MusicManager()
    
    static func addTimeObserver(interval: CMTime = CMTimeMakeWithSeconds(0.01, 100),
                                queue :DispatchQueue = DispatchQueue.main,
                                callBack: @escaping (CMTime) -> Void ) -> Any {
        
        return self.shared.player.addPeriodicTimeObserver(forInterval: interval, queue: queue, using: callBack)
    }
    
    static func addObserver(_ observer: Observable) {
        self.shared.observers.append(ObserverHolder(observer))
    }
    
    static func removeObserver() {
        self.shared.observers = self.shared.observerHolders()
    }
    
    static func seek(to: Float) {
        self.shared.player.seek(to: CMTime(seconds: Double(to), preferredTimescale: 1))
        self.shared.observerHolders().forEach{ $0.observer?.notify(with: .resumed) }
    }
    
    static func getMaxDuration() -> Float {
        return Float(self.shared.player.currentItem?.duration.seconds ?? 0)
    }
    
    static func togglePlay(at index: Int? = nil) {
        let items = self.shared.modelItems
        guard items.count > 0 else { return }
        
        if let currentIndex = self.shared.currentItemIndex, (index == nil || currentIndex == index) {
            self.shared.resumeOrPauseCurrentAudioItem()
            return
        }
        
        if let index = index, items.count > index {
            self.shared.playSong(item: items[index])
            return
        }
    }
    
    static func nextSong() {
        guard let index = self.shared.currentItemIndex, self.shared.modelItems.count > 0 else {return}
        let newIndex = index + 1
        
        if newIndex >= self.shared.modelItems.count {
            self.togglePlay(at: 0)
            return
        }
        
        self.togglePlay(at: newIndex)
    }
    
    static func previousSong() {
        guard let index = self.shared.currentItemIndex, self.shared.modelItems.count > 0 else {return}
        let newIndex = index - 1
        
        if let item = self.shared.player.currentItem, item.currentTime().seconds > 3 {
            self.shared.restartSong()
            return
        }
        
        if newIndex < 0 {
            self.shared.restartSong()
            return
        }
        
        self.togglePlay(at: newIndex)
    }
    
    static func getModel(completion: @escaping ((DataModel?) -> Void), errorBlock: @escaping ((NetworkError?) -> Void) ) {
        DataRequest().send { (model, error) in
            guard let model = model as? DataModel, error == nil else { return errorBlock(error) }
            MusicManager.shared.modelItems = model.items
            completion(model)
        }
    }
    
    static func currentIndex() -> Int? {
        return self.shared.currentItemIndex
    }
    
    static func isMusicPlaying() -> Bool {
        return self.shared.isPlaying
    }
    
    static func currentModel() -> DataItem {
        let index = self.shared.currentItemIndex ?? 0
        return self.shared.modelItems[index]
    }
}
