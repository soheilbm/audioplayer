//
//  ObserverHelpers.swift
//  AudioPlayer
//
//  Created by Soheil on 1/4/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

protocol Observable: class, NSObjectProtocol {
    func notify(with: PlayerStatus)
}

final class ObserverHolder: NSObject {
    weak var observer: Observable?
    init(_ observer: Observable){
        self.observer = observer
    }
}
