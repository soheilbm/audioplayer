//
//  NetworkManager.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

typealias NetWorkResponse = ((Model?, NetworkError?) -> Void)

public enum NetworkError: Error {
    case error(Error?)
    case parsingError
    case wrongRequest
}

enum ApiType: String {
    case get  = "GET"
    case post = "POST"
    case put  = "PUT"
}

fileprivate struct NetworkManager {
    fileprivate static let shared = NetworkManager()
    private init() {}
    private let session = URLSession.shared
    
    fileprivate func fetch(apiRequest: ApiRequest, completionResponse: @escaping NetWorkResponse) {
        guard let url = URL(string: apiRequest.endpoint) else {
            completionResponse(nil,.wrongRequest)
            return
        }
        
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        request.httpMethod = apiRequest.type.rawValue
        
        debugPrint(request)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            guard let data = data else { return completionResponse(nil,.error(error)) }
            
            do{
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? AnyParse else {
                    return completionResponse(nil,.parsingError)
                }
                
                let parser = Parser(json)
                let handler = apiRequest.handler.init(data: parser)
                
                guard let modelResponse = handler.model else {return completionResponse(nil,.parsingError) }
                completionResponse(modelResponse, nil)
            }catch {
                completionResponse(nil,.parsingError)
            }
            
        }).resume()
    }
    
    // This is just for debuging
    private func debugPrint(_ request: URLRequest) {
        guard let method = request.httpMethod, let url = request.url else { return }

        #if DEBUG
            var headerString: String = ""
            if let headers = request.allHTTPHeaderFields {
                headers.forEach {
                    headerString.append("-H '\($0): \(String.init(describing: $1))' ")
                }
            }
            var dataString: String = ""
            if let body = request.httpBody {
                if let bodyString = String(data: body, encoding: String.Encoding.utf8) {
                    dataString = "-d '\(bodyString)'"
                }
            }
            
            print("curl -X \(method.uppercased()) \(headerString)\(dataString) '\(url.absoluteString)'")
        #endif
    }
}

protocol Model {}
protocol ModelHandler {
    var model: Model? {get}
    init(data: Parser)
}

protocol ApiRequest {
    var endpoint: String { get }
    var type: ApiType { get }
    var handler: ModelHandler.Type { get }
}

extension ApiRequest {
    var type: ApiType { return ApiType.get }
    
    func send(completionBlock: @escaping NetWorkResponse) {
        NetworkManager.shared.fetch(apiRequest: self, completionResponse: completionBlock)
    }
}
