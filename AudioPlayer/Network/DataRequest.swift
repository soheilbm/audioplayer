//
//  DataRequest.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

struct DataRequest: ApiRequest {
    var endpoint: String = "https://gist.githubusercontent.com/anonymous/fec47e2418986b7bdb630a1772232f7d/raw/5e3e6f4dc0b94906dca8de415c585b01069af3f7/57eb7cc5e4b0bcac9f7581c8.json"
    var handler: ModelHandler.Type = DataHandler.self
    init() {}
}



fileprivate extension DataModel {
    init?(data: Parser) {
        guard let dataArray = data["data"] as? Array<Any> else { return nil }
        var items = [DataItem]()
        
        for i in dataArray {
            guard let row = i as? AnyParse, let item = DataItem(Parser(row)) else { continue }
            items.append(item)
        }
        
        guard !items.isEmpty else {return nil}
        self.items = items
    }
}

fileprivate extension DataItem {
    init?(_ data: Parser) {
        guard let _id = data["id"] as? String,
            let _name = data["name"] as? String,
            let _audioLink = data["audioLink"] as? String,
            let _authorData = data["author"] as? AnyParse,
            let _picture = data["picture"] as? AnyParse else { return nil }
        
        id = _id
        name = _name
        audioLink = _audioLink
        author = Author(Parser(_authorData))
        createdOn = TimeHelper.getDate(from: data["createdOn"] as? String)
        modifiedOn = TimeHelper.getDate(from: data["modifiedOn"] as? String)
        picture = Picture(Parser(_picture))
    }
}

fileprivate extension Author {
    init(_ data:Parser) {
        self.name = data["name"] as? String
        
        if let _picture = data["picture"] as? AnyParse {
            picture = Picture(Parser(_picture))
        }else{
            picture = nil
        }
    }
}

fileprivate struct DataHandler: ModelHandler {
    var model: Model?
    init(data: Parser) {
        self.model = DataModel(data: data)
    }
}
