//
//  DataModel.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

struct DataModel: Model {
    let items: [DataItem]
}

struct DataItem: Model, Hashable {
    public var hashValue: Int {
        return Int(id.hash)
    }

    let id: String
    let author: Author
    let createdOn: Date?
    let modifiedOn: Date?
    let picture: Picture?
    let audioLink: String
    let name: String
    
    static func ==(_ lhs: DataItem, _ rhs: DataItem) -> Bool {
        return lhs.id == rhs.id && lhs.createdOn == rhs.createdOn && lhs.createdOn == rhs.createdOn && lhs.audioLink == rhs.audioLink && lhs.name == rhs.name
    }
}

struct Author: Model {
    let name: String?
    let picture: Picture?
}

struct Picture: Model {
    enum SizeType {
        case s
        case m
        case xs
        case url
    }
    
    private struct SizeModel {
        private let size: SizeType
        private let url: String
        
        init(_ size: SizeType, _ url: String) {
            self.size = size
            self.url = url
        }
        
        fileprivate func url(with size: SizeType) -> String? {
            guard self.size == size else { return nil}
            return self.url
        }
    }
    
    private let sizes: [SizeModel]
    
    init(_ data: Parser?) {
        var sizes = [SizeModel]()
        
        if let url = data?["s"] as? String { sizes.append(SizeModel(.s, url)) }
        if let url = data?["m"] as? String { sizes.append(SizeModel(.m, url)) }
        if let url = data?["xs"] as? String { sizes.append(SizeModel(.xs, url)) }
        if let url = data?["url"] as? String { sizes.append(SizeModel(.url, url)) }
        self.sizes = sizes
    }
    
    func getImage(with size: SizeType) -> String? {
        for i in sizes {
            if let val = i.url(with: size) {
                return val
            }
        }
        
        return nil
    }
}
