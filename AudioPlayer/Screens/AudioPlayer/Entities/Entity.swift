//
//  Entity.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

struct PlayerViewItem {
    let id: String?
    let authorName: String?
    let audioLink: String?
    let artworkUrl: String?
    let title: String?
    var duration: Float?
    var currentPosition: Float?
    var isPlaying = false
    
    init(_ data: DataItem) {
        self.id = data.id
        self.authorName = data.author.name
        self.audioLink = data.audioLink
        self.artworkUrl = data.picture?.getImage(with: .m)
        self.title = data.name
    }
}
