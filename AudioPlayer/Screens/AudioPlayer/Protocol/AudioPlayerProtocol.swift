import Foundation

protocol AudioPlayerApiProtocol: class {}

protocol AudioPlayerDataProtocol: class {}

protocol AudioPlayerInteractorInputProtocol: class {
    weak var presenter: AudioPlayerInteractorOutputProtocol? { get set }
    func playNext()
    func playPrevious()
    func playOrPause()
    func getCurrentPlayingItem() -> PlayerViewItem?
    func updateDuration()
    func seekToPosiion(position: Float)
}

protocol AudioPlayerAnalyticsProtocol: class {}

protocol AudioPlayerInteractorOutputProtocol: class {
    func outputToPresenter()
    func updateSliderPosition(_ value: Float)
    func updateSliderWithDuration(_ max: Float)
}

protocol AudioPlayerPresenterProtocol: class {
    var interactor: AudioPlayerInteractorInputProtocol? { get set }
}
