import UIKit
import AVFoundation

final class AudioPlayerViewManager: NSObject {
    var dismissView: ((Void) -> Void)?
    var playPauseButton: ((Void) -> Void)?
    var playNext: ((Void) -> Void)?
    var playPrevious: ((Void) -> Void)?
    var seekToPosition: ((Float) -> Void)?
    
    lazy var player: Player = {
        let view = Bundle.main.loadNibNamed("Player", owner: self, options: nil)!.first as! Player
        view.resetState()
        return view
    }()
    
    @IBAction func closedButtonPressed(_ sender: Any) {
        dismissView?()
    }
    
    @IBAction func playNextPressed(_ sender: Any) {
        playNext?()
        player.resetState()
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        playPauseButton?()
    }
    
    @IBAction func previousButtonPressed(_ sender: UIButton) {
        playPrevious?()
        player.resetState()
    }
    
    @IBAction func sliderChanged(_ sender: Any) {
        seekToPosition?(player.slider.value)
        player.isDragging = false
    }
    
    func resetState() {
        player.resetState()
    }
    
    func updateView(_ data: PlayerViewItem?) {
        player.title.text = data?.title
        player.artist.text = data?.authorName
        
        player.setupSliderValue(data?.currentPosition ?? 0)
        player.loadArtwork(data?.artworkUrl)
        player.updateButtonState(data?.isPlaying ?? false)
    }
}

final class Player: UIView {
    @IBOutlet weak var artwork: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var artist: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var mainButton: UIButton!
    var isDragging = false
    
    func updateElapseTime(_ value: Float) {
        guard isDragging == false else { return }
        slider.value = value
        startLabel.text = "\(value)"
    }
    
    func updateSlider(_ max: Float) {
        slider.isEnabled = true
        slider.minimumValue = 0.0
        slider.maximumValue = max
        startLabel.text = "\(0.0)"
        endLabel.text = "\(max)"
    }
    
    @IBAction func touchdown(_ sender: Any) {
        isDragging = true
    }
    
    @IBAction func starDragging(_ sender: UISlider) {
        isDragging = true
    }
    
    @IBAction func exitDragging(_ sender: UISlider) {
        isDragging = false
    }
    
    func resetState() {
        slider.value = 0.0
        slider.minimumValue = 0
        slider.maximumValue = 1
        slider.isEnabled = false
        startLabel.text = ""
        endLabel.text = ""
    }
    
    func loadArtwork(_ str: String?) {
        artwork.download(str)
    }
    
    func setupSliderValue(_ float: Float){
        slider.value = float
    }
    
    func updateButtonState(_ status: Bool) {
        
        if status {
            self.mainButton.setImage(#imageLiteral(resourceName: "Pause"), for: UIControlState.normal)
        }else {
            self.mainButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
        }
    }
}
