import UIKit

struct AudioPlayerRoute: RouterModuleProtocol {
    var screen: UIViewController {
        let presenter = AudioPlayerPresenter()
        let interactor:AudioPlayerInteractorInputProtocol = AudioPlayerInteractor()

        presenter.interactor = interactor
        interactor.presenter = presenter

        return presenter
    }
}


