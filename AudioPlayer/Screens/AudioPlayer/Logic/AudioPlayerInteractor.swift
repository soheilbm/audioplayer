import Foundation
import AVFoundation

final class AudioPlayerInteractor: NSObject, Observable, AudioPlayerInteractorInputProtocol {
    weak var presenter: AudioPlayerInteractorOutputProtocol?
    var startPoint = 0.0
    var endPoint = 0.0
    var observer: Any?
    
    override init() {
        super.init()
        MusicManager.addObserver(self)
        self.observer = MusicManager.addTimeObserver(callBack: timeElapsed)
    }
    
    deinit {
        self.observer = nil
        MusicManager.removeObserver()
    }
    
    func notify(with: PlayerStatus) {
        guard let _ = MusicManager.currentIndex() else { return }
        
        if with == .startPlaying {
            updateDuration()
            presenter?.outputToPresenter()
        }
    }
    
    func seekToPosiion(position: Float){
        MusicManager.seek(to: position)
    }
    
    func updateDuration() {
        presenter?.updateSliderWithDuration(MusicManager.getMaxDuration())
    }
    
    func timeElapsed(_ time: CMTime) -> Void {
        presenter?.updateSliderPosition(Float(time.seconds))
    }
    
    func playNext(){
        MusicManager.nextSong()
    }
    
    func playPrevious() {
        MusicManager.previousSong()
    }
    
    func playOrPause(){
        MusicManager.togglePlay()
        presenter?.outputToPresenter()
    }
    
    func getCurrentPlayingItem() -> PlayerViewItem? {
        let item = MusicManager.currentModel() 
        var newItem = PlayerViewItem(item)
        newItem.isPlaying = MusicManager.isMusicPlaying()
        return newItem
    }
}
