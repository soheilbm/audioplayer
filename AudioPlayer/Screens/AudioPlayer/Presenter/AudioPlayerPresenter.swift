import UIKit

final class AudioPlayerPresenter: UIViewController, AudioPlayerPresenterProtocol, AudioPlayerInteractorOutputProtocol {
    var interactor: AudioPlayerInteractorInputProtocol?
    var viewManager: AudioPlayerViewManager = AudioPlayerViewManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white

        viewManager.dismissView = { [weak self] in self?.dismissView() }
        viewManager.playNext = interactor?.playNext
        viewManager.playPrevious = interactor?.playPrevious
        viewManager.playPauseButton = interactor?.playOrPause
        viewManager.player.frame = view.bounds
        viewManager.seekToPosition = interactor?.seekToPosiion
        
        view.addSubview(viewManager.player)
        viewManager.player.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        interactor?.updateDuration()
        outputToPresenter()
    }

    private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateSliderPosition(_ value: Float) {
        viewManager.player.updateElapseTime(value)
    }
    
    func updateSliderWithDuration(_ max: Float) {
        viewManager.player.updateSlider(max)
    }
    
    // MARK: AudioPlayerInteractorOutputProtocol
    func outputToPresenter() {
        viewManager.updateView(interactor?.getCurrentPlayingItem())
    }
}
