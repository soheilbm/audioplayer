//
//  MainViewPresenter.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

final class MainViewPresenter: UIViewController, MainViewPresenterProtocol {
    var interactor: MainViewInteractorInputProtocol?
    lazy var viewManager: MainViewViewManager = MainViewViewManager()
    lazy var activityView: UIActivityIndicatorView = {
        let aView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        aView.startAnimating()
        return aView
    }()
    
    lazy var statusBar: UIView = {
        let visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
        return visualEffectView
    }()
    
    fileprivate func stopActivityIndicator() {
        activityView.stopAnimating()
        activityView.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        activityView.center = view.center
        view.addSubview(activityView)
        
        viewManager.itemAtIndex = item
        viewManager.numberOfItems = totalItems
        viewManager.selectItem = selectItem
        viewManager.presentAudioPlayer = presentAudioPlayer
        viewManager.isItemPlaying = isItemPlaying
        interactor?.loadData()
    }
    
    func presentMiniBar(item: MainViewItem){
        if !viewManager.isMinibarVisible { viewManager.toggleMiniBar() }
        viewManager.updateMiniBar(model: item)
    }
    
    func hideMiniBAr(){
        if viewManager.isMinibarVisible { viewManager.toggleMiniBar() }
    }
    
    func updateViews() {
        guard let interactor = interactor else { hideMiniBAr(); return }
        
        if let item = interactor.getCurrentPlayingItem(), interactor.isMusicPlaying() {
            presentMiniBar(item: item)
        }else{
            hideMiniBAr()
        }
        
        viewManager.reloadTable()
    }
        
    private func item(_ index: Int) -> MainViewItem? {
        guard let items = interactor?.entity.items else { return nil }
        return items[index]
    }
    
    private func isItemPlaying(_ index: Int) -> Bool {
        return interactor?.isMusicPlaying(at: index) ?? false
    }
    
    private func selectItem(_ index: Int?) {
        interactor?.toggleMusic(at: index)
    }
    
    private func totalItems() -> Int {
        return interactor?.entity.items.count ?? 0
    }
    
    func updateViewManager() {
        self.stopActivityIndicator()
        
        viewManager.tableView.frame = view.bounds
        view.addSubview(viewManager.tableView)
        viewManager.tableView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        viewManager.reloadTable()
        
        viewManager.minibar.frame = CGRect(x: 0, y: view.bounds.height, width: view.bounds.width, height: 55)
        view.addSubview(viewManager.minibar)
        
        view.addSubview(statusBar)
    }
}

extension MainViewPresenter: MainViewInteractorOutputProtocol {
    func dataLoaded() {
        print("Items loaded")
        
        DispatchQueue.main.async {
            self.updateViewManager()
        }
    }

    
    func failedToLoadData() {
        // failed to load TODO show some error
        print("failed to load data")
    }
    
    func presentAudioPlayer() {
        Router.shared.present(.AudioPlayer)
    }
}
