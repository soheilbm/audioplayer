//
//  MainViewRoute.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

struct MainViewRoute: RouterModuleProtocol {
    var screen: UIViewController {
        let presenter = MainViewPresenter()
        let interactor:MainViewInteractorInputProtocol = MainViewInteractor()
        let apiManager: MainViewApiProtocol = MainViewApiManager()

        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.apiManager = apiManager

        return presenter 
    }
}
