//
//  MainViewProtocol.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

protocol MainViewApiProtocol: class {
    func loadData(completionBlock: @escaping ((MainViewEntity?) -> Void)) 
}

protocol MainViewDataProtocol: class {
    
}

protocol MainViewInteractorInputProtocol: class {
    var apiManager: MainViewApiProtocol? { get set }
    weak var presenter: MainViewInteractorOutputProtocol? { get set }
    var entity: MainViewEntity { get }
    func loadData()
    func isMusicPlaying() -> Bool
    func toggleMusic(at index: Int?)
    func isMusicPlaying(at index: Int) -> Bool
    func getCurrentPlayingItem() -> MainViewItem? 
}

protocol MainViewAnalyticsProtocol: class {}

protocol MainViewInteractorOutputProtocol: class {
    func dataLoaded()
    func failedToLoadData()
    func presentAudioPlayer()
    func updateViews()
    func presentMiniBar(item: MainViewItem)
    func hideMiniBAr()
}

protocol MainViewPresenterProtocol: class {
    var interactor: MainViewInteractorInputProtocol? { get set }
}
