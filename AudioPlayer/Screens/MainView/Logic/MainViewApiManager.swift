//
//  MainViewApiManager.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

final class MainViewApiManager: MainViewApiProtocol {
    func loadData(completionBlock: @escaping ((MainViewEntity?) -> Void)) {
        
        MusicManager.getModel(completion: { (model) in
            completionBlock(MainViewEntity(model))
        }) { (error) in
            completionBlock(nil)
        }
    }
}
