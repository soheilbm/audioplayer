//
//  MainViewInteractor.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

final class MainViewInteractor: NSObject, Observable, MainViewInteractorInputProtocol {
    var apiManager: MainViewApiProtocol?
    var analytics: MainViewAnalyticsProtocol? 
    weak var presenter: MainViewInteractorOutputProtocol?
    var entity = MainViewEntity()

    override init() {
        super.init()
        MusicManager.addObserver(self)
    }
    
    deinit {
        MusicManager.removeObserver()
    }
    
    func notify(with: PlayerStatus) {
        guard let index = MusicManager.currentIndex() else { return }
        
        if with == .startPlaying || with == .resumed  || with == .paused {
            presenter?.presentMiniBar(item: entity.items[index])
        }
        
        if with == .paused {
            presenter?.updateViews()
        }
    }
    
    func loadData() {
        apiManager?.loadData(completionBlock: { [weak self] entity in
            guard let entity = entity else { self?.presenter?.failedToLoadData(); return }
            self?.entity = entity
            self?.presenter?.dataLoaded()
        })
    }
        
    func isMusicPlaying(at index: Int) -> Bool {
        if let i = MusicManager.currentIndex(), i == index, MusicManager.isMusicPlaying() {
            return true
        }
        
        return false
    }
    
    func getCurrentPlayingItem() -> MainViewItem? {
        if let i = MusicManager.currentIndex() {
            return entity.items[i]
        }
        return nil
    }
    
    func isMusicPlaying() -> Bool {
        return MusicManager.isMusicPlaying()
    }
    
    func toggleMusic(at index: Int?){
        MusicManager.togglePlay(at: index)
    }
}
