//
//  MainViewEntity.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import Foundation

struct MainViewEntity {
    let items: [MainViewItem]
    
    init(_ model: DataModel? = nil) {
        var newItems = [MainViewItem]()
        
        
        model?.items.forEach{ newItems.append(MainViewItem($0)) }
        
        self.items = newItems
    }
}

struct MainViewItem {
    let id: String?
    let authorName: String?
    let authorAvatar: String?
    let audioLink: String?
    let artworkUrl: String?
    let lastUpdate: String?
    let title: String?
    
    init(_ data: DataItem) {
        self.id = data.id
        self.authorName = data.author.name
        self.authorAvatar = data.author.picture?.getImage(with: .s)
        self.audioLink = data.audioLink
        self.artworkUrl = data.picture?.getImage(with: .m)
        self.lastUpdate = TimeHelper.toDateTime(data.modifiedOn)
        self.title = data.name
    }
}
