//
//  MainViewViewManager.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

final class MainViewViewManager: NSObject {
    var numberOfItems: ((Void) -> Int)?
    var itemAtIndex: ((Int) -> MainViewItem?)?
    var selectItem: ((Int?) -> Void)?
    var isItemPlaying: ((Int) -> Bool)?
    var presentAudioPlayer: ((Void) -> Void)?
    var isMinibarVisible = false
    
    lazy var minibar: MiniPlayer = {
        let bar = Bundle.main.loadNibNamed("MiniPlayer", owner: nil, options: nil)!.first as! MiniPlayer
        bar.toggleMusic = { [weak self] _ in self?.selectItem?(nil) }
        bar.showMediaPlayer = { [weak self] _ in self?.presentAudioPlayer?() }
        return bar
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.register(UINib(nibName: "ArtworkTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    func updateMiniBar(model: MainViewItem) {
        minibar.topLabel.text = model.title
        minibar.bottomLabel.text = model.authorName
    }
        
    func toggleMiniBar() {
        isMinibarVisible = !isMinibarVisible
        let position: CGFloat = (isMinibarVisible) ? -55 : 0
        let top = CGAffineTransform(translationX: 0, y: position)
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            self.minibar.transform = top
        }, completion: nil)
    }
    
    func reloadTable() {
        print("reload Items")
        tableView.reloadData()
    }
}

extension MainViewViewManager: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ArtworkTableViewCell.rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfItems?() ?? 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected == false, let val = isItemPlaying?(indexPath.row), val == true {
            cell.setSelected(true, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ArtworkTableViewCell
        cell.selectionStyle = .none
        
        if let item = itemAtIndex?(indexPath.row) {
            cell.titleLabel.text = item.title
            cell.topLabel.text = item.title
            cell.artistName.text = item.authorName
            cell.updatedTime.text = item.lastUpdate
            cell.loadAvatar(item.authorAvatar)
            cell.loadArtwork(item.artworkUrl)
            
            cell.itemSelected = { [weak self] in
                let isSelected = cell.selectedItem
                self?.unselectAllCells()
                cell.setSelected(!isSelected, animated: false)
                self?.selectItem?(indexPath.row)
            }
        }
        
        return cell
    }
    
    func unselectAllCells() {
        for cell in tableView.visibleCells {
            cell.setSelected(false, animated: false)
        }
    }
}
