//
//  MiniPlayer.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

final class MiniPlayer: UIView {
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    var isActiveForPlaying = false
    var toggleMusic: ((Void) -> Void)?
    var showMediaPlayer: ((Void) -> Void)?
    
    func showActiveState() {
        self.mainButton.setImage(#imageLiteral(resourceName: "Pause"), for: UIControlState.normal)
    }
    
    private func updateButtonState() {
        if isActiveForPlaying {
            self.mainButton.setImage(#imageLiteral(resourceName: "Pause"), for: UIControlState.normal)
        }else {
            self.mainButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
        }
    }
    
    @IBAction func playPauseButtonPressed(_ sender: UIButton) {
        self.isActiveForPlaying = !isActiveForPlaying
        updateButtonState()
        toggleMusic?()
    }
    
    @IBAction func containerButtonPressed(_ sender: UIButton) {
        showMediaPlayer?()
    }
}
