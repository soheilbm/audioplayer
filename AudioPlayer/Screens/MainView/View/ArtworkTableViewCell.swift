//
//  ArtworkTableViewCell.swift
//  AudioPlayer
//
//  Created by Soheil on 27/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

class ArtworkTableViewCell: UITableViewCell {
    static var rowHeight: CGFloat = 350
    
    @IBOutlet weak var artworkImage: UIImageView!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistAvatar: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var updatedTime: UILabel!
    @IBOutlet weak var musicIcon: UIImageView!
    private var isActiveForPlaying: Bool = false
    var itemSelected: ((Void) -> Void)?
    var selectedItem = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.artistAvatar.clipsToBounds = true
        self.artistAvatar.layer.cornerRadius = artistAvatar.frame.width / 2
    }
    
    func resetToOriginalState() {
        self.mainButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
        self.musicIcon.isHidden = true
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            self.mainButton.setImage(#imageLiteral(resourceName: "Pause"), for: UIControlState.normal)
            self.musicIcon.isHidden = false
        }else {
            self.mainButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
            self.musicIcon.isHidden = true
        }
        
        self.selectedItem = selected
    }
    
    func loadAvatar(_ str: String?) {
        artistAvatar.download(str)
    }
    
    func loadArtwork(_ str: String?) {
        artworkImage.download(str)
    }

    @IBAction func buttonPressed(_ sender: UIButton) {
        itemSelected?()
    }
}
