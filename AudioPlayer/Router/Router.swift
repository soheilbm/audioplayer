//
//  Router.swift
//  AudioPlayer
//
//  Created by Soheil on 26/3/17.
//  Copyright © 2017 soheil. All rights reserved.
//

import UIKit

protocol RouterModuleProtocol  {
    var screen: UIViewController {get}
}

enum Screens {
    case mainView
    case AudioPlayer
    
    fileprivate var getScreen: UIViewController {
        switch self {
        case .mainView:
            return MainViewRoute().screen
        case .AudioPlayer:
            return AudioPlayerRoute().screen
        }
    }
    
    fileprivate static func `default`() -> Screens {
        return .mainView
    }
}

final class Router {
    static let shared = Router()
    private init() {}
    private var rootController: UIViewController?
    
    func setupRootView(with launchOption: [UIApplicationLaunchOptionsKey: Any]? = nil) -> UIWindow {
        setupRootScreen()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = rootController
        window.backgroundColor = UIColor.white
        window.makeKeyAndVisible()
        return window
    }
    
    private func setupRootScreen() {
        let screen = Screens.default().getScreen
        screen.view.frame = UIScreen.main.bounds
        let nav = UINavigationController(rootViewController: screen)
        nav.setNavigationBarHidden(true, animated: false)
        self.rootController = nav
    }
    
    func present(_ screen: Screens) {
        DispatchQueue.main.async {
            let vc = screen.getScreen
            self.rootController?.present(vc, animated: true, completion: nil)
        }
    }
}
